#!/usr/bin/python3

import os
import re
import argparse
import logging
import pathlib
import pyexcel as pe
import pysubs2
from constants import LANGUAGE_CODES

# logging.basicConfig(filename='example.log',level=logging.DEBUG)
logging.basicConfig(level=logging.INFO)


def subs_time_str(subsms):
    return "{:02d}:{:02d}:{:02d},{:03d}".format(*pysubs2.time.ms_to_times(subsms))


def write_srt(f, subs_strings):
    logging.info('Write SubRip file : {}'.format(f))
    subs = pysubs2.SSAFile.from_string(''.join(subs_strings))
    subs.save(f, encoding="utf-8", format_="srt")


def read_excel(f, col_name):
    logging.info('Read excel file : {}'.format(f))
    subs_sheet = pe.get_sheet(file_name=f, auto_detect_int=False ,auto_detect_float=False)
    logging.info('lang_name = {}'.format(col_name))
    if col_name is not None and len(col_name) > 0:
        logging.info('find the lang_col of language {} in {}'.format(col_name, subs_sheet.row[0]))
        # lang_col = subs_sheet.row[0].index(col_name.upper())
        try:
            lang_col = subs_sheet.row[0].index(col_name.upper())
            if len(subs_sheet.column[lang_col]) != len(subs_sheet.column[0]):
                logging.error('Translation is not completed!!')
                raise ValueError
        except (ValueError, IndexError):
            logging.error('No such translation !!')
            return 0, ""
    else:
        lang_col = 2

    i = 1
    subs_strings = []
    terminate = 0
    last_rd = None

    for rd in subs_sheet:
        if terminate > 3:
            break
        if not rd[0] or len(rd[0]) != 12:
            terminate += 1
            continue

        # print('{}\tSTART={}\tEND={}\tTEXT={}, type={}'.format(i, rd[0], rd[1], rd[lang_col], type(rd[lang_col])))
        # continue
        if i > 1:
            pre_end_ts = pysubs2.time.timestamp_to_ms(pysubs2.time.TIMESTAMP.match(last_rd[1]).groups())
            start_ts = pysubs2.time.timestamp_to_ms(pysubs2.time.TIMESTAMP.match(rd[0]).groups())
            if not rd[lang_col]:
                subs_strings[-1] = subs_strings[-1].replace(last_rd[1], rd[1])
            else:
                subs_text = int(rd[lang_col]) if type(rd[lang_col]) is float else rd[lang_col]
                if pre_end_ts >= start_ts:
                    # print("overlapping substitle !!!,  {} >= {}".format(last_rd[1], rd[0]))
                    pre_start_ts = pysubs2.time.timestamp_to_ms(pysubs2.time.TIMESTAMP.match(last_rd[0]).groups())
                    if start_ts - pre_start_ts > 50:
                        # print('update substitile 1, old = {}'.format(subs_strings[-1]))
                        subs_strings[-1] = subs_strings[-1].replace(last_rd[1], subs_time_str(start_ts - 10))
                        # print('update substitile 1, new = {}'.format(subs_strings[-1]))
                    else:
                        # print('update substitile 2, old = {}'.format(subs_strings[-1]))
                        subs_strings[-1] = subs_strings[-1].replace(last_rd[1], subs_time_str(start_ts))
                        # print('update substitile 2, new = {}'.format(subs_strings[-1]))
                    subs_strings.append('\n{}\n{} --> {}\n{}\n'.format(i, rd[0], rd[1], subs_text))
                else:
                    subs_strings.append('\n{}\n{} --> {}\n{}\n'.format(i, rd[0], rd[1], subs_text))
        else:
            subs_text = int(rd[lang_col]) if type(rd[lang_col]) is float else rd[lang_col]
            subs_strings.append('{}\n{} --> {}\n{}\n'.format(i, rd[0], rd[1], subs_text))
        i += 1
        last_rd = rd
    return i, subs_strings


def read_srt(f):
    fp = '{}'.format(f)
    logging.info('Read SRT file: {}'.format(fp))
    csv_data = []
    subs = pysubs2.load(fp, format='srt', encoding="utf-8")
    csv_data.append(['START', 'END', 'ENGLISH'])
    # print(type(subs)) 'pysubs2.ssafile.SSAFile'
    i = 0
    for subl in subs:
        if i > 0:
            csv_data.append([subs_time_str(subl.start), subs_time_str(subl.end), subl.text])
        i += 1

    return csv_data


def azure_translate_text(target, text):
    """Translates text into the target language by Azure

    [Supported language code]
    (https://docs.microsoft.com/en-us/azure/cognitive-services/translator/languages)
    [Text API 3.0: Translate]
    (ttps://docs.microsoft.com/en-us/azure/cognitive-services/translator/reference/v3-0-translate?tabs=curl)
    [Quickstart: Translate text with Python]
    (https://docs.microsoft.com/en-us/azure/cognitive-services/translator/quickstart-python-translate)
    """
    import http.client, uuid, json
    # Replace the subscriptionKey string value with your valid subscription key.
    subscription_key = 'da4510a68322419ba5d869a8fb2386ff'

    host = 'api.cognitive.microsofttranslator.com'
    path = '/translate?api-version=3.0'
    params = "&to=zh-Hant";     # target
    request_body = [{
        'Text': text,
    }]

    def transliterate(content):
        headers = {
            'Ocp-Apim-Subscription-Key': subscription_key,
            'Content-type': 'application/json',
            'X-ClientTraceId': str(uuid.uuid4())
        }
        conn = http.client.HTTPSConnection(host)
        conn.request("POST", path + params, content, headers)
        response = conn.getresponse()
        return response.read()

    content = json.dumps(request_body, ensure_ascii=False).encode('utf-8')
    result = transliterate(content)
    json_reply = json.loads(result)
    print(json.dumps(json_reply, indent=4, ensure_ascii=False))
    if type(json_reply) is list:
        json_dict = json_reply[0]
    else:
        json_dict = json_reply
    if type(json_dict['translations']) is list:
        for json_data in json_dict['translations']:
            if json_data['to'] == "zh-Hant":
                return json_data['text']
    else:
        print('{} -- {}'.format(json_dict['translations']['to'], json_dict['translations']['text']))
        return json_dict['translations']['text']

    return "ERROR, No Translated Text"

    # Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
    # We want to avoid escaping any Unicode characters that result contains. See:
    # https://stackoverflow.com/questions/18337407/saving-utf-8-texts-in-json-dumps-as-utf8-not-as-u-escape-sequence
    # output = json.dumps(json.loads(result), indent=4, ensure_ascii=False)
    # print(output)


def google_translate_text(target, text):
    """Translates text into the target language by Google.

    Target must be an ISO 639-1 language code.
    See https://g.co/cloud/translate/v2/translate-reference#supported_languages
    """
    from google.cloud import translate
    # import six
    translate_client = translate.Client()

    # if isinstance(text, six.binary_type):
    #     text = text.decode('utf-8')

    # Text can also be a sequence of strings, in which case this method
    # will return a sequence of results for each text.
    result = translate_client.translate(
        text, target_language=target)

    # print(u'Text: {}'.format(result['input']))
    # print(u'Translation: {}'.format(result['translatedText']))
    # print(u'Detected source language: {}'.format(result['detectedSourceLanguage']))
    return result['translatedText']


def read_en_srt_google_translate(f, dst):
    logging.info('Read SRT file: {}'.format(f))
    csv_data = []
    subs = pysubs2.load(f, format='srt', encoding="utf-8")
    csv_data.append(['START', 'END', 'ENGLISH', '繁體中文'])
    # print(type(subs)) 'pysubs2.ssafile.SSAFile'
    i = 0
    for subl in subs:
        if i > 0:
            csv_data.append(
                [subs_time_str(subl.start), subs_time_str(subl.end), subl.text, google_translate_text('zh-TW', subl.text)])
        i += 1

    return csv_data


def read_en_srt_azure_translate(f, dst):
    logging.info('Read SRT file: {}'.format(f))
    csv_data = []
    subs = pysubs2.load(f, format='srt', encoding="utf-8")
    csv_data.append(['START', 'END', 'ENGLISH', '繁體中文'])
    # print(type(subs)) 'pysubs2.ssafile.SSAFile'
    i = 0
    for subl in subs:
        if i > 0:
            csv_data.append(
                [subs_time_str(subl.start), subs_time_str(subl.end), subl.text, azure_translate_text('zh-Hant', subl.text)])
        i += 1

    return csv_data


def get_match_lang_code(name):
    for code, lang_name in sorted(LANGUAGE_CODES.items()):
        if type(lang_name) is list:
            for n in lang_name:
                if name.upper() == n.upper():
                    print('matched name = {}'.format(n))
                    return code, n
        elif name.upper() == lang_name.upper():
            return code, lang_name
    return "", ""


def get_lang_code_name(target):
    '''
    :param target:
    :return: lang_code, lang_name
    '''
    if not target:
        print("Invalid argument !!")
        return '', ''

    logging.debug('checking target language: {}'.format(target))
    lang_code, lang_name = get_match_lang_code(target)
    if not lang_code:
        logging.debug('checking args.lang as language code: {}'.format(target))
        lang = LANGUAGE_CODES.get(target)
        if lang:
            lang_code = target
            lang_name = lang[0] if type(lang) is list else lang
        else:
            return 'en', 'ENGLISH'

    if not lang_code:
        logging.info('Cannot find matched language target " {} ".'.format(target))
        disp_lang = input('List all supported languages (Y/N) :')
        if disp_lang and "Y" in disp_lang.upper():
            for code, language in sorted(LANGUAGE_CODES.items()):
                logging.info('{:7s}\t{}'.format(code, language))
            return '', ''
        else:
            logging.info('Set default lang_code = "en", and lang_name = "ENGLISH"')
            return 'en', 'ENGLISH'
    else:
        return lang_code, lang_name


def validate_lnag(target_lang):
    if not target_lang:
        return False
    else:
        lang_code, lang_name = get_match_lang_code(target_lang)
        if not lang_code:
            print("No a valid language code !!!, Set as default 'ENGLISH'")
            return False
        else:
            return True


def get_file_list(args):
    '''
    Get the list of those transcript files to be processed.
    :param args:
    :return: infile_list:  a list of pathlib objects.
             lang: the language of the transcript files.
    '''
    infile_list = []
    target = pathlib.Path(args.filepath)
    if target.is_dir():
        current_working_dir = target.absolute().name
        if '_' in current_working_dir:
            lang = current_working_dir.split('_')[-1]
        else:
            lang = 'ENGLISH'
        print('current_working_dir = {}, lang = {}'.format(current_working_dir, lang))

        if args.SRT:
            if args.csv:
                search_suffix = "*.csv"
            else:
                search_suffix = "*.xlsx"
        elif args.CC:
            search_suffix = "*.*"
        else:
            search_suffix = "*.srt"

        for f in target.glob(search_suffix):
            if f.is_file():
                infile_list.append(f)

    elif target.is_file():
        current_working_dir = target.absolute().parent.name
        ext = target.suffix
        print('current_working_dir = {}, suffix = {}'.format(current_working_dir, ext))
        infile_list.append(target)
        if '_' in target.stem:
            lang = target.stem.split('.')[0].split('_')[-1]
        elif '_' in current_working_dir:
            lang = current_working_dir.split('_')[-1]
        else:
            lang = 'ENGLISH'

    print("WTRC target-lang: {}".format(lang))
    if not validate_lnag(lang):
        lang = 'ENGLISH'

    return infile_list, lang


def check_save_path(save_path):
    '''
    :param save_path: A export_dir object of a Path class
    :return: export_dir: A export_dir object of a Path class
    '''
    export_dir = pathlib.Path(str(save_path))
    if not export_dir.exists() or not export_dir.is_dir():
        try:
            export_dir.mkdir()
        except FileExistsError:
            print('Export dir "{}" is not existed or the target dir is a file'.format(export_dir))
    if export_dir.exists():
        return export_dir
    else:
        return None


def convertSRT2WebVtt(fp, output_dir):
    '''
    Required package webvtt-py https://pypi.org/project/webvtt-py/
    :param fp:
    :param output_dir:
    :return:
    '''
    import webvtt
    subs_webvtt = webvtt.from_srt(str(fp))
    save_as_file = output_dir / '{}.vtt'.format(fp.stem)
    print('Convering {}'.format(save_as_file))
    subs_webvtt.save(str(save_as_file))


# def parse_srt(f):
#     import srt
#     with open(f) as srt_lines:
#         subs = list(srt.parse(srt_lines))
#         for line in subs:
#             print(line)


def arg_parse():
    parser = argparse.ArgumentParser(description='Transform SRT to XLSX/CSV or reverse. \n\
                                                 The default target language is English.\n\
                                                 If the file or folder name has _LANGUAGE suffix, the target language \
                                                 will be detected by the suffix.')
    parser.add_argument('filepath', help='The path to the transcript / subtitle files')
    parser.add_argument('--SRT', default=False, action='store_true',
                        help='Transform XLSX/CSV to SRT, otherwise Transform SRT to EXCEL.')
    parser.add_argument('--WebVTT', default=False, action='store_true',
                        help='Convert SRT to WebVTT.')
    parser.add_argument('--csv', default=False, action='store_true', help='Read or Write CSV format, otherwise XLSX.')
    parser.add_argument('--translate', default=False, action='store_true', help='Enable auto translation')
    parser.add_argument('--SRC', default="en", help='the source language')
    parser.add_argument('--DST', default="zh-TW", help="the destination language. If it's specified, do auto-translation.")
    parser.add_argument('--engine', default="google", help='Auto translation engine, only google now')
    parser.add_argument('--export', default="", help='Specify export directory')
    parser.add_argument('--CC', default=False, action='store_true',
                        help='Chinese conversion by the suffix of path, utilized opencc')
    # parser.add_argument('--lang_code', default="", help='The translated language code')
    args = parser.parse_args()
    print("argument filepath   : {}".format(args.filepath))
    print("argument SRT        : {}".format(args.SRT))
    print("argument WebVTT     : {}".format(args.WebVTT))
    print("argument csv        : {}".format(args.csv))
    print("argument translate  : {}".format(args.translate))
    print("argument SRC        : {}".format(args.SRC))
    print("argument DST        : {}".format(args.DST))
    print("argument engine     : {}".format(args.engine))
    print("argument export     : {}".format(args.export))
    print("argument CC         : {}".format(args.CC))
    # print("argument lang_code  : {}".format(args.lang_code))
    return args


def main_proc():
    args = arg_parse()
    infile_list, lang_suffix = get_file_list(args)
    if not infile_list:
        print('\nNo valid files to be converted !!\n')
        return
    elif not lang_suffix:
        print('\nNo valid language suffix !!\n')
        return
    else:
        print('transcript files count = {}, lang_suffix = {}'.format(len(infile_list), lang_suffix))

    lang_code, lang_name = get_lang_code_name(lang_suffix)
    if not lang_code:
        print('\nNo valid language target !!\n')
        return
    else:
        print('Target language code "{}" and name "{}"'.format(lang_code, lang_name))

    if args.SRT:
        if not args.export:
            export_dir = infile_list[0].parent / '..' / 'SRT_{}'.format(lang_name.upper())
            export_dir = check_save_path(export_dir)
        else:
            export_dir = check_save_path(args.export)
        print('export_dir = {}, lang_code = {}'.format(export_dir, lang_code))
        export_srt_count = 0
        for fp in infile_list:
            try:
                lines, sub_strs = read_excel(str(fp), lang_name)
                if lines > 1:
                    # if lang_suffix in fp.stem:
                    #     fname = re.sub(r' *- *', '_', fp.stem).replace(lang_suffix, lang_code, 1)
                    #     output = export_dir / '{}'.format(fname)
                    # else:
                    #     output = export_dir / '{}_{}'.format(fp.stem, lang_code)
                    fname = re.sub(r'Wondercise_\d+\.\d+\.\d+_', '', fp.name)
                    fname = re.sub(r'_*{}_*'.format(lang_suffix), '_', fname)
                    fname = fname.replace('_.', '.')
                    fname = fname.replace('_', ' ')
                    # fname = re.sub(r' .', '', fname)
                    print("WTRC-11- fname: {}".format(fname))
                    
                    output =  export_dir / '{}_{}.srt'.format(fname, lang_code)
                    print("WTRC-22- output: {}".format(output))
                    write_srt(str(output), sub_strs)
                    export_srt_count += 1
            except ...:
                import sys
                print('Unexpected error: {}'.format(sys.exc_info()[0]))
            print('export_srt_count = {}'.format(export_srt_count))
    elif args.WebVTT:
        export_dir = infile_list[0].parent / '..' / 'WebVTT_{}'.format(lang_name.upper())
        export_dir = check_save_path(export_dir)
        print('export_dir = {}, lang_code = {}'.format(export_dir, lang_code))
        for fp in infile_list:
            try:
                convertSRT2WebVtt(fp, export_dir)
            except ...:
                import sys
                print('Unexpected error: {}'.format(sys.exc_info()[0]))
    elif args.CC:
        if lang_code == "zh-Hant":
            print('convert to simplified chinese')
            cc = "t2s"
        elif lang_code == "zh-Hans":
            print('convert to simplified chinese')
            cc = "s2t"
        else:
            print('No valid chinese language, {}'.format(lang_name))
            return
        #ToDo utilize OpenCC to convert the chinese in xlsx, csv, or txt
        #from opencc import OpenCC
        # export_dir = infile_list[0].parent / '..' / 'SRT_{}'.format(lang_name.upper())
        # export_dir = check_save_path(export_dir)
        # print('export_dir = {}, lang_code = {}'.format(export_dir, lang_code))
        # for fp in infile_list:
        #     try:
        #         chinese_converter(fp, export_dir)
        #     except ...:
        #         import sys
        #         print('Unexpected error: {}'.format(sys.exc_info()[0]))
    else:
        if args.translate:
            export_dir = infile_list[0].parent / 'xlsx_繁體中文'
        else:
            export_dir = infile_list[0].parent / 'xlsx'
        export_dir = check_save_path(export_dir)
        print('export_dir = {}'.format(export_dir))
        export_count = 0
        for fp in infile_list:
            if args.translate:
                # csv_data = read_en_srt_google_translate(fp, args.DST)
                csv_data = read_en_srt_azure_translate(fp, args.DST)
            else:
                csv_data = read_srt(fp)
            if args.csv:
                pe.save_as(array=csv_data, dest_file_name='{}/{}.csv'.format(export_dir, fp.stem))
            else:
                pe.save_as(array=csv_data, dest_file_name='{}/{}.xlsx'.format(export_dir, fp.stem))
            export_count += 1
        print('export_count = {}'.format(export_count))


if __name__ == '__main__':
    print("\n")
    main_proc()
    print("\n")

"""
    History:
    2018.Jun.19  Add WebVTT support
"""

